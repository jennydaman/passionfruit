export default function (config, env, helpers) {
  config.output.publicPath = env.production ? '/passionfruit/' : '';
}

