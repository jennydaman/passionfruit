import style from './style.css';
import { Plus } from 'preact-feather';

const EventCard = ({ imageUrl, eventTitle, color, date, description, ...props }) => (
  <div {...props}>
    <div class={style.card}>
      <div style={`background-image: url(${imageUrl})`} className={style.cardImage}>
        <h2 className={style.eventTitle}>{eventTitle}</h2>
        <div className={style.cardFab} style={`background-color: ${color}`}>
          <Plus size={28} color="white" />
      </div>
      </div>
      <div className={style.cardBody}>
        <p className={style.eventDate}>{date}</p>
        <p>{description}</p>
      </div>
    </div>
  </div>
);

export default EventCard;
