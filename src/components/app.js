import { h, Component } from 'preact';
import { Router } from 'preact-router';
import { createHashHistory } from 'history';
import Header from './header';

// Code-splitting is automated for routes
import Home from '../routes/home';
import Connections from '../routes/connections';
import Community from '../routes/community';
import Profile from '../routes/connections/me';
import ConnectionForm from '../routes/connections/new';

const PAGE_NAME = {
  '/': '',
  '/connection': 'Connections',
  '/connection/me': 'Your Connection',
  '/connection/new': 'New Connection',
  '/community': 'Community'
};


export default class App extends Component {

	state = {
	  displayTitle: 'Passion Fruit'
	};

	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => {
	  this.currentUrl = e.url;
	  this.setState({ displayTitle: PAGE_NAME[this.currentUrl] });
	  this.pageTitle = this.currentUrl;
	};

	render(props, { displayTitle }) {
	  return (
	    <div id="app">
	      <Header displayTitle={displayTitle} />
	      <main style="padding: 56px 0px; height: 100%">
	        <Router history={createHashHistory()} onChange={this.handleRoute}>
	          <Home path="/" />
	          <Connections path="/connection" />
	          <Profile path="/connection/me" />
	          <ConnectionForm path="/connection/new" />
	          <Community path="/community" />
	        </Router>
	      </main>
	    </div>
	  );
	}
}
