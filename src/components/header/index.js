import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

// TODO connections activeClassName doesn't work because of redirect
const Header = ({ displayTitle, ...props }) => (
  <header class={style.header} {...props} >
    <h1>{displayTitle}</h1>
    <nav>
      {/*<Link activeClassName={style.active} href="/">Home</Link>*/}
      <Link activeClassName={style.active} href="/connection">Connections</Link>
      <Link activeClassName={style.active} href="/community">Community</Link>
    </nav>
  </header>
);

export default Header;
