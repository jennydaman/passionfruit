import { h, Component } from 'preact';
import style from './style.css';
import { route } from 'preact-router';

export default class ConnectionForm extends Component {

	state = {
	  inputName: ''
	};

	addNameToStorage = e => {
	  window.sessionStorage.setItem('login', this.state.inputName);
	};

	render({ ...props }, { ...state }) {
	  return (
	    <div {...props} id={style.centerForm}>
	      <form id={style.formBlock} action="/connection/me" onSubmit={this.addNameToStorage} method="GET">
	        <h3>Name</h3>
	        <input type="text" name="name" required
	          onChange={e => this.setState({ inputName: e.target.value })}
	        />
	        <div class={style.separator} />

	        <p>
	          <h3>Your Match Preferences</h3>
	          <input type="radio" name="volunteer" value="veteran" />
						Meet a former veteran<br />
	          <input type="radio" name="volunteer" value="student" />
						Anyone (middle school students to volunteers of any age)<br />
	        </p>

	        <p>
	          <h4>Gender</h4>
	          <input type="radio" name="gender" value="male" />
						Male<br />
	          <input type="radio" name="gender" value="female" />
						Female<br />
	          <input type="radio" name="gender" value="none" />
						no preference<br />
	        </p>
	        <input type="submit" style="margin-top: 1em" />
	      </form>
	    </div>
	  );
	}
}
