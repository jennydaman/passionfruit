import { h, Component } from 'preact';
import style from './style';
import { User, Calendar } from 'preact-feather';

export default class Profile extends Component {

  componentWillMount() {
    // for stateful wireframes, hide the GET query string
    let uri = window.location.toString();
    uri = uri.substring(0, uri.indexOf('?'));
    window.history.replaceState({}, document.title, uri);
    this.name = window.sessionStorage.getItem('login');
  }

  render(props) {
    return (
      <div {...props} class="text-section">
        <p>
          Welcome back, {this.name}!
        </p>
        <p>
          Your partner is:
        </p>
        <p>
          <div class={style.avatarCard}>
            <div class={style.avatarCircle}>
              <User color="white" size={48} />
            </div>
            <div class={style.avatarDetails}>
              <b>Minie Zhang</b>
              <br />
              <small>(xxx) xxx-xxxx</small>
            </div>
          </div>
          <div class={style.detailsExtension}>
            Volunteer (college student)
          </div>
        </p>
        <p>
          <h2>
            <Calendar style="margin-right: 0.5em" />
          Get-Togethers
          </h2>
          <p class={style.getTogethers}>
            <small>Aug 10</small>
            <br />
            Grocery shopping and cooking!
            <br />
            &#9;10:00 meet at grocery store
            <br />
            ...
            <div class={style.separator}></div>
          </p>
        </p>
      </div>
    );
  }
}
