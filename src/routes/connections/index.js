import { h, Component } from 'preact';
import { route } from 'preact-router';

// https://github.com/preactjs/preact-router#redirects
export default class Connections extends Component {
  componentWillMount() {
    const sessionStorage = window.sessionStorage;
    if (sessionStorage.getItem('login'))
      route('/connection/me', true);
    else
      route('/connection/new', true);
  }
  render() {
    return null;
  }
}
