import { h, Component } from 'preact';
import style from './style.css';
import EventCard from '../../components/eventcard';

export default class Community extends Component {

  data = [
    {
      imageUrl: 'http://assets1.ignimgs.com/2017/02/23/yz4a2309-1487857203836_1280w.jpg',
      eventTitle: 'LAN Party',
      color: '#FF0266',
      date: 'Sep 16',
      description: 'First of many get-togethers at the VA Medical Center!'
    },
    {
      imageUrl: 'http://1.bp.blogspot.com/-nm3chh563rI/T56c1zt78jI/AAAAAAAAAxk/LfdjD8bTToo/s1600/good+spread.jpg',
      eventTitle: 'VA Potluck',
      color: '#43A047',
      date: 'Oct 9',
      description: 'What\'s cookin good lookin'
    },
    {
      imageUrl: 'http://shedsblueprints.com/wp-content/uploads/2013/06/woodworking-6.jpg',
      eventTitle: 'Intro to Woodworking',
      color: '#ffeb3b',
      date: 'Oct 20',
      description: 'Ever wanted to try woodworking? Get your safety glasses and let\'s make something creative.'
    }
  ];

  render({ ...props }, { ...state }) {
    const eventsCards = this.data.map(a => <EventCard {...a} class={style.card}/>);
    return (
      <div id={style.eventsAlignLeft}>
        {eventsCards}
      </div>
    );
  }
}
