import { h, Component } from 'preact';
import style from './style';

export default class Community extends Component {
  // Note: `user` comes from the URL, courtesy of our router
  render({ ...props }, { ...state }) {
    return (
      <div {...props}>
				Community page
      </div>
    );
  }
}
