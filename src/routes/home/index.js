import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

const Home = ({ ...props }) => (
  <div id={style.home} {...props} >

    <div id={style.coverPhoto}>
      <h1 id={style.title}>Passion Fruit</h1>
      <div class={style.shortLine} />
      <div id={style.buttonsRow}>
        <Link class={style.button} href="/connection">
          <span>Connections</span>
        </Link>
        <Link class={style.button} href="/community">
          <span>Community</span>
        </Link>
      </div>
    </div>

    <section class="text-section">
      <h1>About Us</h1>
      <p><em>Passion Fruit</em> brings veterans together with their local community.</p>
    </section>
  </div>
);

export default Home;
